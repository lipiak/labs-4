﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2B
{
    public class ComponentDwaBe :AbstractComponent, IDrzwi
    {
        public ComponentDwaBe()
        {
            this.RegisterProvidedInterface(typeof(IDrzwi), this);
        }

        static void Main(string[] args)
        {
        }

        public string Otworz()
        {
            return "sprawdzam zabezpieczenia";
        }

        public string Zamknij()
        {
            return "drzwi aktualnie zamkniete";
        }

        public static IDrzwi pobierzDrzwi()
        {
            return new ComponentDwaBe();
        }



        public override void InjectInterface(Type type, object impl)
        {
            
        }

       
    }
}
