﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Contract
{
    public interface ILogika
    {
        void AkcjaGuzika();
        void OdetnijZasilanie();
    }
}
