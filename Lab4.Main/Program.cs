﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using Lab4.Component2;
using Lab4.Component1;
using ComponentFramework;
using Lab4.Component2B;

namespace Lab4.Main
{
    public class Program
    {
        static IContainer kontener;
        static void Main(string[] args)
        {
            kontener = new Container();
            
        }

        public static void zarejestrujComponent(IContainer cont, IComponent comp)
        {
            cont.RegisterComponent(comp);
        }

        public static bool czyPowiazaniaZrobione(IContainer cont)
        {
            return cont.DependenciesResolved;
        }
    }
}
