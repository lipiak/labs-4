﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Logika :AbstractComponent, ILogika
    {
        IDrzwi drzwi;

        public Logika()
        {
            this.RegisterRequiredInterface(typeof(IDrzwi));
        }

        static void Main(string[] args)
        {
        }

        public IDrzwi pobierzDrzwi()
        {
            return drzwi;
        }

        public static Logika pobierzLogike()
        {
            return new Logika();
        }


        public static ILogika zwrocLogike(IDrzwi drzwi)
        {
            return new Logika();
        }



        public void AkcjaGuzika()
        {
            
        }

        public void OdetnijZasilanie()
        {

        }


        public override void InjectInterface(Type type, object impl)
        {
            if (type is IDrzwi)
           {
                this.drzwi = (IDrzwi)impl;
            }
        }

        //public  void InjectInterface(Type type, object impl)
        //{
        //    if (type is IDrzwi)
        //    {
        //        this.drzwi = (IDrzwi)impl;
        //    }
        //}

        
    }
}
