﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Drzwi :AbstractComponent, IDrzwi
    {
        static void Main(string[] args)
        {
        }

        public Drzwi()
        {
            this.RegisterProvidedInterface(typeof(IDrzwi),this);
        }

        public string Otworz()
        {
            return "otwieram";
        }

        public string Zamknij()
        {
            return "zamykam";
        }

        public static IDrzwi zwrocDrzwi()
        {
            return new Drzwi();
        }



        public override void InjectInterface(Type type, object impl)
        {
            
        }

        
    }
}
